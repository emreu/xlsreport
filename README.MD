# Usage
Mark structure fields with tag `column:"Header Title"` to display them in table.

To produce multilevel header with hierarchy separate each level with `|` character. Like `Super Header|Sub Header|Sub-sub Header`.
Consecutive fields with similar header titles will merge under common headers.

Use tag `opt:"avg"` or `opt:"sum"` or `opt:"avg sum"` to calculate statistics on column.


# Usage example

```go
import (
	"fmt"
	"xlsreport"
)

type Record struct {
	Name        string `column:"Name"`
	Age         int    `column:"Age"`
	MobilePhone string `column:"Phone|Mobile"`
	LocalNumber string `column:"Phone|Internal"`
	CallTime    string `column:"Total Call time" opt:"sum avg"`
	LocCalls    int    `column:"Calls|Local" opt:"sum"`
	ExtCalls    int    `column:"Calls|External" opt:"sum"`
}

func main() {
	report := xlsreport.New("")

	table := report.AddTable("Sample report table")

	table.AddRow(Record{"John Smith", 32, "(555)1234567", "256", "00:49:30", 10, 3})
	table.AddRow(Record{"Jon Snow", 20, "(555)3337777", "404", "00:05:17", 1, 0})
	table.AddRow(Record{"Lo Wang", 43, "(555)7654321", "512", "01:20:00", 15, 8})

	err := report.GenerateAndSave("results.xlsx")
	if err == nil {
		fmt.Println("Ok!")
	} else {
		fmt.Println(err)
	}
}
```
