package xlsreport

import (
	"io"

	"github.com/tealeg/xlsx"
)

// ReportItem interface
type ReportItem interface {
	Draw(doc *xlsx.File) error
}

// Report structure
type Report struct {
	Title string
	items []ReportItem
}

// New create new Report
func New(Title string) (r *Report) {
	r = &Report{Title: Title}
	return
}

// AddTable create empty table in report and return it for further operations
func (r *Report) AddTable(title string) *Table {
	table := &Table{Title: title}
	r.items = append(r.items, table)
	return table
}

func (r *Report) generate() *xlsx.File {
	doc := xlsx.NewFile()
	for _, item := range r.items {
		item.Draw(doc)
	}
	return doc
}

// GenerateToWriter generate report content and stream it to writer
func (r *Report) GenerateToWriter(w io.Writer) error {
	doc := r.generate()
	return doc.Write(w)
}

// GenerateAndSave generate report content and save it to file
func (r *Report) GenerateAndSave(fname string) error {
	doc := r.generate()
	return doc.Save(fname)
}
