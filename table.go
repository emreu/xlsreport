package xlsreport

import (
	"errors"
	"fmt"

	"github.com/tealeg/xlsx"

	"reflect"
	"strings"
	// "pdfreport/utils"
)

// Table object
type Table struct {
	Title       string
	rows        int
	avgrow      bool
	sumrow      bool
	columns     []*column
	header      *columnHeader // Headers tree structure for faster rendering
	dataType    reflect.Type  // Type of structs displayed in table
	headerDepth int
}

type columnHeader struct {
	title       string
	w           float64
	subHeaders  []*columnHeader
	col         *column
	childrenNum int
}

type column struct {
	fieldIndex int // Index of field in structure associated with this column
	maxWidth   float64
	values     []string
	showSum    bool
	showAvg    bool
	aggr       aggregator
}

// initTable create columns and headers from structure tags
func (t *Table) initTable(dataType reflect.Type) error {
	if dataType.Kind() != reflect.Struct {
		return errors.New("type is not a struct")
	}
	t.dataType = dataType // Lock table's struct type
	// rightmost edge column hierarchy
	hcolumn := []*columnHeader{new(columnHeader)}
	for i := 0; i < dataType.NumField(); i++ {
		colName := dataType.Field(i).Tag.Get("column")
		if colName == "" {
			continue
		}
		// build headers hierarchy
		for level, subtitle := range strings.Split(colName, "|") {
			if len(hcolumn) > level+2 && hcolumn[level+1].title == subtitle {
				hcolumn[level+1].childrenNum++
				continue
			}
			colHeader := &columnHeader{title: subtitle}
			hcolumn[level].subHeaders = append(hcolumn[level].subHeaders, colHeader)
			hcolumn = append(hcolumn[:level+1], colHeader)
			if level > t.headerDepth {
				t.headerDepth = level
			}
		}
		// check aggregation options
		opts := dataType.Field(i).Tag.Get("opt")
		sum := strings.Contains(opts, "sum")
		avg := strings.Contains(opts, "avg")
		if avg {
			t.avgrow = true
		}
		if sum {
			t.sumrow = true
		}
		column := &column{fieldIndex: i, showSum: sum, showAvg: avg}
		if sum || avg {
			var aggr aggregator

			switch dataType.Field(i).Type.Kind() {
			case reflect.String:
				aggr = new(durationAggregator)
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				aggr = new(intAggregator)
			case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
				aggr = new(intAggregator)
			case reflect.Float32, reflect.Float64:
				aggr = new(floatAggregator)
			}

			column.aggr = aggr
		}
		hcolumn[len(hcolumn)-1].col = column
		t.columns = append(t.columns, column)
	}
	t.header = hcolumn[0]
	return nil
}

func (t *Table) addrow(v reflect.Value) {
	for _, col := range t.columns {
		text := fmt.Sprint(v.Field(col.fieldIndex))
		col.values = append(col.values, text)
		maxWidth := 30.0
		if maxWidth > col.maxWidth {
			col.maxWidth = maxWidth
		}
		if col.aggr != nil {
			col.aggr.Append(v.Field(col.fieldIndex))
		}
		// TODO: aggregate values
	}
	t.rows++
}

// AddRow add single row to table. Row should be struct with fields marked with tag `column`.
// Empty table accept any structure, but lock it type after successful addition.
// All consecutive AddRow and AddRows should be called with the same struct type.
func (t *Table) AddRow(row interface{}) error {
	val := reflect.ValueOf(row)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	if t.dataType == nil {
		if err := t.initTable(val.Type()); err != nil {
			return err
		}
	} else if t.dataType != val.Type() {
		return errors.New("inconsistent struct type") // Don't try to mix different types in single table
	}
	t.addrow(val)
	return nil
}

// AddRows add multiple rows to table. Rows should be array or slice of structs (see AddRow).
// After first successful addition all consecutive calls should provide same struct type.
func (t *Table) AddRows(rows interface{}) error {
	val := reflect.ValueOf(rows)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	if val.Kind() != reflect.Slice && val.Kind() != reflect.Array {
		return errors.New("rows is not array or slice of structs")
	}
	if t.dataType == nil {
		if err := t.initTable(val.Type().Elem()); err != nil {
			return err
		}
	} else if t.dataType != val.Type().Elem() {
		return errors.New("inconsistent struct type") // Don't try to mix different types in single table
	}

	for i := 0; i < val.Len(); i++ {
		t.addrow(val.Index(i))
	}
	return nil
}

func (t *Table) layout() {
	// // first pass: set columns width, find max depth and children number
	// var traverse func(*columnHeader) (float64, int)
	// traverse = func(header *columnHeader) (width float64, int) {
	// 	// header.title = strEncoder(header.title)
	// 	// width = t.fontmeter.GetStringWidth(header.title) + 2
	// 	if header.col != nil {
	// 		width = math.Max(width, header.col.maxWidth+2)
	// 	} else {
	// 		childrenWidth := float64(0)
	// 		for _, subheader := range header.subHeaders {
	// 			cWidth, cDepth := traverse(subheader)
	// 			childrenWidth += cWidth
	// 			if cDepth > header.childrenDepth {
	// 				header.childrenDepth = cDepth
	// 			}
	// 		}
	// 		width = math.Max(width, childrenWidth)
	// 	}
	// 	header.w = width
	// 	return width, header.childrenDepth + 1
	// }
	// fullWidth, maxDepth := traverse(t.header)
	// t.headerDepth = maxDepth
	//
	// // second pas: scale widths and set height
	// var traverse2 func(*columnHeader, float64, int)
	// traverse2 = func(header *columnHeader, width float64, depth int) {
	// 	if depth > 0 {
	// 		header.h = hBase * float64(maxDepth-depth) / float64(header.childrenDepth+1)
	// 	}
	// 	header.w = width
	// 	if header.col != nil {
	// 		header.col.maxWidth = width
	// 	} else {
	// 		// calculate total children width (may be less then parent width)
	// 		totalWidth := float64(0)
	// 		for _, subheader := range header.subHeaders {
	// 			totalWidth += subheader.w
	// 		}
	// 		// distribute available width between children according to their relative widths
	// 		for _, subheader := range header.subHeaders {
	// 			traverse2(subheader, width*subheader.w/totalWidth, depth+1)
	// 		}
	// 	}
	// }
	// traverse2(t.header, t.header.w, 0)
}

func (t *Table) drawHeader(s *xlsx.Sheet) {
	hdrRows := make([]*xlsx.Row, t.headerDepth+1)
	for i := 0; i < t.headerDepth+1; i++ {
		hdrRows[i] = s.AddRow()
	}

	hstyle := xlsx.NewStyle()
	hstyle.Font.Bold = true
	hstyle.Alignment.Horizontal = "center"
	hstyle.Alignment.Vertical = "center"
	hstyle.Alignment.WrapText = true
	hstyle.Fill.PatternType = "solid"
	hstyle.Fill.FgColor = "FFD9D9D9"
	hstyle.Border = *xlsx.NewBorder("thin", "thin", "thin", "thin")

	var traverse func(*columnHeader, int)
	traverse = func(header *columnHeader, level int) {
		if level >= 0 {
			cell := hdrRows[level].AddCell()
			cell.Value = header.title
			cell.SetStyle(hstyle)
			if header.childrenNum > 0 {
				cell.HMerge = header.childrenNum
				for i := header.childrenNum; i > 0; i-- {
					hdrRows[level].AddCell()
				}
			}

			if len(header.subHeaders) == 0 && level < t.headerDepth {
				cell.VMerge = t.headerDepth - level
				for l := level; l < t.headerDepth; l++ {
					hdrRows[l+1].AddCell()
				}
			}
		}

		for _, subheader := range header.subHeaders {
			traverse(subheader, level+1)
		}
	}
	traverse(t.header, -1)
}

// Draw generate pdf from table
func (t *Table) Draw(doc *xlsx.File) error {
	t.layout()
	sheet, err := doc.AddSheet(t.Title)
	if err != nil {
		return err
	}
	row := sheet.AddRow()
	row.AddCell().Value = t.Title
	t.drawHeader(sheet)

	for i := 0; i < t.rows; i++ {
		row := sheet.AddRow()
		for _, col := range t.columns {
			cell := row.AddCell()
			// TODO: Set proper cell type
			cell.Value = col.values[i]
		}
	}

	if t.avgrow {
		row := sheet.AddRow()
		for j, col := range t.columns {
			cell := row.AddCell()
			if col.aggr != nil {
				cell.Value = fmt.Sprint(col.aggr.Mean(t.rows))
			}
			if j == 0 {
				cell.Value = "Average"
			}
		}
	}

	if t.sumrow {
		row := sheet.AddRow()
		for j, col := range t.columns {
			cell := row.AddCell()
			if col.aggr != nil {
				cell.Value = fmt.Sprint(col.aggr.Sum())
			}
			if j == 0 {
				cell.Value = "Sum"
			}
		}
	}
	return nil
}
