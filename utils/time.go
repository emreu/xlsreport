package utils

import (
	"fmt"
	"time"
	"strings"
)

func ParseTime(sometime string) time.Time {
	parsed, err := time.Parse("02.01.2006 15:04", sometime)
	if err != nil {
		fmt.Println("time parsing error: ", err)
	}
	return parsed
}
func FormatDuration(seconds int64) string {

	ss := seconds % 60
	seconds /= 60
	mm := seconds % 60
	seconds /= 60
	hh := seconds % 24

	return fmt.Sprintf("%02d:%02d:%02d", hh, mm, ss)
}

func ParseDuration(duration string) int64 {
	duration = strings.Replace(duration, ":", "%s", -1)
	parsed, err := time.ParseDuration(fmt.Sprintf(duration, "h", "m") + "s")
	if err != nil{
		panic(err.Error())
	}

	return int64(parsed.Seconds())
}