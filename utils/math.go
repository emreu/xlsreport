package utils

import (
	"math"
)

func Avg(piece int64, total int64) float64 {
	if total == 0 {
		return 0
	}
	return Round(DivisionByZero((float64(piece)), float64(total)), 0.5, 2)
}

func Round(val float64, roundOn float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	if div >= roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	newVal = round / pow
	return
}

func Percent(piece int64, total int64) float64 {
	if total == 0 {
		return 0
	}
	return Round(DivisionByZero((float64(piece)*100), float64(total)), 0.5, 2)
}

func DivisionByZero(num float64, devider float64) float64 {
	if devider == 0 {
		return 0
	}
	return num / devider
}
