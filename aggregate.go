package xlsreport

import (
	"reflect"
	"xlsreport/utils"
)

//Aggregator interface
type aggregator interface {
	Append(reflect.Value)
	Sum() reflect.Value
	Mean(count int) reflect.Value
}

type durationAggregator int64

func (d *durationAggregator) Append(val reflect.Value) {
	seconds := utils.ParseDuration(val.String())

	*d += durationAggregator(seconds)
}

func (d *durationAggregator) Sum() reflect.Value {
	return reflect.ValueOf(utils.FormatDuration(int64(*d)))
}

func (d *durationAggregator) Mean(count int) reflect.Value {
	mean := int64(utils.DivisionByZero(float64(*d), float64(count)))
	return reflect.ValueOf(utils.FormatDuration(mean))
}

type intAggregator int64

func (i *intAggregator) Append(val reflect.Value) {
	*i += intAggregator(val.Int())
}

func (i *intAggregator) Sum() reflect.Value {
	return reflect.ValueOf(int64(*i))
}

func (i *intAggregator) Mean(count int) reflect.Value {
	mean := int64(utils.DivisionByZero(float64(*i), float64(count)))
	return reflect.ValueOf(mean)
}

type floatAggregator float64

func (f *floatAggregator) Append(val reflect.Value) {
	*f += floatAggregator(val.Float())
}

func (f *floatAggregator) Sum() reflect.Value {
	return reflect.ValueOf(float64(*f))
}

func (f *floatAggregator) Mean(count int) reflect.Value {
	mean := utils.DivisionByZero(float64(*f), float64(count))
	return reflect.ValueOf(mean)
}
